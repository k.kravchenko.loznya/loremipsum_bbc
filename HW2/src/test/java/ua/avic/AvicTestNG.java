package ua.avic;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static org.testng.Assert.assertTrue;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertEquals;


public class AvicTestNG {

    private WebDriver driver;

    @BeforeTest
    public void profileSetUp(){
        System.setProperty("webdriver.chrome.driver","src\\main\\resources\\chromedriver.exe" );
    }
    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://avic.ua/");
    }

   @Test (priority = 3)
   public void checkTitle() {
        String actualTitle = driver.getTitle();
        String expectedTitle = "AVIC™ - удобный интернет-магазин бытовой техники и электроники в Украине. | Avic";
        Assert.assertEquals(actualTitle, expectedTitle);

    }
    @Test (priority = 2)
    public void checkProductAvailability() {
        driver.findElement(xpath("//input[@id='input_search']")).sendKeys("236606");
        driver.findElement(xpath("//button[@class='button-reset search-btn']")).click();
        driver.findElement(xpath("//div[@class = 'prod-cart height']")).click();
        String actualAvailability =
                driver.findElement(xpath("//div[@class = 'prod-status']")).getText();
        assertEquals(actualAvailability, "Есть на складе");
    }

        @Test(priority = 2)
        public void checkAmountOfElementsOnSearchPage() {
            driver.findElement(xpath("//input[@id='input_search']")).sendKeys("Моноколесо");
            driver.findElement(xpath("//button[@class='button-reset search-btn']")).click();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            List<WebElement> listOfElements = driver.findElements(xpath("//div[@class= 'row js_more_content js_height-block']"));
            int actualListSize = listOfElements.size();
            assertEquals(actualListSize, 1);
        }
        @Test(priority = 1)
        public void checkThatUrlContainsSearchWord() {
            driver.findElement(xpath("//input[@id='input_search']")).sendKeys("Razer Kraken Kitty");
            driver.findElement(xpath("//button[@class='button-reset search-btn']")).click();
            assertTrue(driver.getCurrentUrl().contains("query=Razer+Kraken+Kitty"));
        }

    @Test (priority = 1)
    public void checkPurchaseWithEmptyCart() {
        driver.findElement(xpath("//div[@class = 'header-bottom__cart active-cart flex-wrap middle-xs js-btn-open']")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(xpath("//div[@data-cart-name = 'modal']/descendant::a[contains(.,'Оформить')]")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        String actualCartIsEmpty = driver.findElement(xpath("//div[@class = 'ttl js_title']")).getText();
        assertEquals(actualCartIsEmpty, "Корзина пустая!");
    }


    @AfterMethod
    public void tearDown() {
        driver.close();
    }

}
