package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RevlonPage extends BasePage{
    public RevlonPage(WebDriver driver) {
        super(driver);
    }

    private String myBid= "8.25";

    @FindBy(xpath = "//input[@id='MaxBidId']")
    private WebElement bidAmount;

    public void placeABid(){
        bidAmount.sendKeys(myBid);
    }
}
