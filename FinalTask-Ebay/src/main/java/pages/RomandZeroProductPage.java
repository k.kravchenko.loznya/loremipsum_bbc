package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RomandZeroProductPage extends BasePage{
    public RomandZeroProductPage(WebDriver driver) {
        super(driver);
    }

    private String number = "4";

    @FindBy (xpath = "//a[@aria-label='See details about shipping']")
    private WebElement seeDetails;
    @FindBy (xpath = "//select[@name='country']")
    private WebElement selectList;
    @FindBy (xpath = "//select[@name='country']/option[71]")
    private WebElement countryChoice;
    @FindBy (xpath = "//input[@value='Get Rates']")
    private WebElement getRatedButton;
    @FindBy (xpath = "//input[@id='shQuantity']")
    private WebElement amountOfProducts;

    public void clickSeeDetails(){
        seeDetails.click();
    }
    public void clickOnSelectList(){
        selectList.click();
    }
    public void selectCountry(){
        countryChoice.click();
    }
    public void clickGetRatedButton(){
        getRatedButton.click();
    }
    public void changeNumberOfProducts(){
        amountOfProducts.clear();
        amountOfProducts.sendKeys(number);
    }
}
