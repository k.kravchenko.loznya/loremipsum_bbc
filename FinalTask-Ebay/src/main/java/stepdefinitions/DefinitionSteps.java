package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;
import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class DefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 70;
    WebDriver driver;
    HomePage homePage;
    CartPage cartPage;
    SearchResultsPage searchResultsPage;
    SellProductPage sellProductPage;
    RomandZeroProductPage romandZeroProductPage;
    RomandJuicyProductPage romandJuicyProductPage;
    SignInPage signInPage;
    RevlonPage revlonPage;
    PageFactoryManager pageFactoryManager;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @Given("User opens homepage page")
    public void openPage() {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }


    @After
    public void tearDown() {
        driver.close();
    }

    @And("User searches by keyword")
    public void userSearchesByKeyword() {
        homePage.searchForProduct();
    }

    @And("User clicks search button")
    public void userClicksSearchButton() {
        homePage.clickSearchButton();
    }

    @When("User chooses product")
    public void userChooseProduct() {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.chooseProduct();
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User clicks on button Buy it now")
    public void userClickOnButtonBuyItNow() {
        romandJuicyProductPage = pageFactoryManager.getRomandJuicyProductPage();
        romandJuicyProductPage.clickOnBuyItNowButton();
        romandJuicyProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        romandJuicyProductPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
    }

    @And("User gets error message")
    public void userGetsErrorMessage() {
        romandJuicyProductPage.getErrorMessage();
        romandJuicyProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User clicks on button Add to cart")
    public void userClickOnButtonAddToCart()  {
        romandJuicyProductPage.clickAddToCartButton();
        romandJuicyProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User clicks on button Add to Wishlist")
    public void userClickOnButtonAddToWishlist() {
        romandJuicyProductPage.clickAddToWishlistButton();
        romandJuicyProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @Then("User refers to sign in page")
    public void userRefersToLogInPage() {
        signInPage = pageFactoryManager.getSignInPage();
        signInPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User sees amount in a Cart")
    public void userSeesAmountInACart() {
        cartPage = pageFactoryManager.getCartPage();
        cartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        cartPage.getNumberInTheCart();
        cartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User click on select")
    public void userClickOnSelect() {
        romandJuicyProductPage = pageFactoryManager.getRomandJuicyProductPage();
        romandJuicyProductPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        romandJuicyProductPage.clickOnSelect();
        romandJuicyProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User chooses from dropdown list")
    public void userChoosesFromDropdownList() {
        romandJuicyProductPage.chooseFromDropDownList();
        romandJuicyProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User click on Checkout button")
    public void userClickOnCheckoutButton() {
        cartPage.clickCheckoutButton();
    }

    @Given("User clicks on the empty Cart")
    public void userClicksOnTheEmptyCart() {
        homePage.clickOnTheCartButton();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User gets message cart is empty")
    public void userGetsMessageCartIsEmpty() {
        cartPage = pageFactoryManager.getCartPage();
        cartPage.getErrorMessageForEmptyCart();
        cartPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User clicks on My Ebay")
    public void userClicksOnMyEbay() {
        homePage = pageFactoryManager.getHomePage();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.clickOnMenuMyEbay();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User clicks on the Watchlist")
    public void userClicksOnTheWatchlist()  {
        homePage = pageFactoryManager.getHomePage();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.clickWatchlistIcon();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("Message pops up about Watchlist")
    public void messagePopsUpAboutWatchlist() {
        homePage.getErrorMessageAboutWatchlist();

    }

    @When("User clicks on the Bell")
    public void userClicksOnTheBell() {
        homePage.clickNotificationButton();
    }

    @Then("Message pops up about notifications")
    public void messagePopsUpAboutNotifications() {
        homePage.getErrorMessageAboutNotification();
    }

    @And("User clicks on Ship to")
    public void userClicksOnShipTo() {
        homePage.clickShipTo();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User clicks on Select")
    public void userClicksOnSelect() {
        homePage.clickToSelectCountryFromList();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @When("User choose country")
    public void userChooseCountry() {
        homePage.selectCountryFromList();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @Then("User press Done")
    public void userPressDone() {
        homePage.clickOnDone();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }


    @And("User opens other product")
    public void userOpensOtherProduct() {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.openProduct();
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User searches by new keyword")
    public void userSearchesByNewKeyword() {
        homePage.searchForNewProduct();
    }

    @And("User opens new product")
    public void userOpensNewProduct() {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.chooseNewProduct();
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User clicks on shipping details")
    public void userClicksOnShippingDetails() {
        romandZeroProductPage = pageFactoryManager.getRomandZeroProductPage();
        romandZeroProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        romandZeroProductPage.clickSeeDetails();
    }

    @When("User selects change country")
    public void userSelectsChangeCountry() {
        romandZeroProductPage.clickOnSelectList();
        romandZeroProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User selects country from dropdown")
    public void userSelectsCountryFromDropdown() {
        romandZeroProductPage.selectCountry();
        romandZeroProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User presses Get rated")
    public void userPressesGetRated() {
        romandZeroProductPage.clickGetRatedButton();
        romandZeroProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User changes Quantity")
    public void userChangesQuantity() {
        romandZeroProductPage.changeNumberOfProducts();
    }

    @And("User clicks on Shop By Category")
    public void userClicksOnShopByCategory() {
        homePage.clickShopByCategoryButton();
    }

    @And("User chooses category")
    public void userChoosesCategory() {
        homePage.clickOnHealthAndBeauty();
    }

    @And("User chooses MakeUp Product")
    public void userChoosesMakeUpProduct() {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.clickOnMakeUp();
    }

    @And("User chooses Lips")
    public void userChoosesLips() {
        searchResultsPage.clickOnLips();
    }

    @And("User chooses Lips stain")
    public void userChoosesLipsStain() {
        searchResultsPage.clickOnLipStain();
    }

    @When("User chooses Action")
    public void userSelectAction() {
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultsPage.selectAction();
    }

    @And("User chooses action to participate")
    public void userChoosesActionToParticipate() {
        searchResultsPage.clickOnRevlonAution();
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User places a bid")
    public void userPlacesABid() {
        revlonPage = pageFactoryManager.getRevlonPage();
        revlonPage.placeABid();
    }

    @And("User clicks on Sell")
    public void userClicksOnSell() {
        homePage.clickOnSell();
    }

    @And("User clicks list an item")
    public void userClicksListAnItem() {
        sellProductPage = pageFactoryManager.getSellProductPage();
        sellProductPage.clickOnListAnItemButton();
    }

    @And("User enters brand name")
    public void userEntersBrandName()  {
        sellProductPage.enterBrandName();
    }

    @And("User click on the pencil")
    public void userClickOnThePencil() {
        sellProductPage.clickOnEditButton();
        sellProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("User adds choice")
    public void userAddsChoice() {
        sellProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        sellProductPage.chooseCategory();
        sellProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @When("User click on view possible matches")
    public void userClickOnViewPossibleMatches() {
        sellProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        sellProductPage.clickOnViewPossibleMatches();
    }

    @And("User click on continue with selection")
    public void userClickOnContinueWithSelection() {
        sellProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        sellProductPage.clickContinueWithSelectionButton();
    }

    @And("User selects condition")
    public void userSelectsCondition() {
        sellProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        sellProductPage.clickOnRadioButtonNew();
    }

    @And("User press Next")
    public void userPressNext() {
        sellProductPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        sellProductPage.clickNextButton();
    }

    @And("User click Search")
    public void userClickSearch()  {
        sellProductPage.clickSearchButton();
    }

    @When("User goes to checkout as guest")
    public void userGoesToCheckoutAsGuest() {
    romandJuicyProductPage.checkoutAsAGuest();
    }

    @And("User chooses country")
    public void userChoosesCountry() {
        cartPage = pageFactoryManager.getCartPage();
        cartPage.selectCountry();
    }

    @And("User enters First Name")
    public void userEntersFirstName() {
        cartPage.setFirstName();
    }

    @And("User enters Last Name")
    public void userEntersLastName() {
        cartPage.setLastName();
    }

    @And("User enters street address")
    public void userEntersStreetAddress() {
        cartPage.setStreetAddress();
    }

    @And("User enters city")
    public void userEntersCity() {
        cartPage.setCity();
    }

    @And("User enters zipcode")
    public void userEntersZipcode() {
        cartPage.setZipcode();
    }

    @And("User enters email")
    public void userEntersEmail() {
        cartPage.setEmail();
    }

    @And("User confirms email")
    public void userConfirmsEmail() {
        cartPage.confirmEmail();
    }

    @And("User chooses country code")
    public void userChoosesCountryCode() {
        cartPage.selectCountryCode();
    }

    @Then("User enters phone number")
    public void userEntersPhoneNumber() {
        cartPage.setPhoneNumber();
    }

    @Then("User press done")
    public void userClickDone() {
        cartPage.clickDone();

    }
}
