Feature: Rom&ndTestSet
  As a user
  I want to check functionality to buy now/add to Cart/add to Wishlist without sign in
  So that all these functions work correctly


  Scenario: Buy now/Add to Cart/ Add to Wishlist without choosing type
    Given User opens homepage page
    And User searches by keyword
    And User clicks search button
    When User chooses product
    And User clicks on button Buy it now
    And User gets error message
    And User clicks on button Add to cart
    And User gets error message
    And User clicks on button Add to Wishlist
    Then User gets error message

  Scenario: Buy it now product without sign in
    Given User opens homepage page
    And User searches by keyword
    And User clicks search button
    And User chooses product
    And User click on select
    And User chooses from dropdown list
    When User clicks on button Buy it now
    Then User goes to checkout as guest


  Scenario: Check Add to Cart without sign in
    Given User opens homepage page
    And User searches by keyword
    And User clicks search button
    And User chooses product
    And User click on select
    And User chooses from dropdown list
    When User clicks on button Add to cart
    And User sees amount in a Cart
    And User click on Checkout button
    Then User refers to sign in page



  Scenario: Add product to Wishlist without sign in
    Given User opens homepage page
    And User searches by keyword
    And User clicks search button
    And User chooses product
    And User click on select
    And User chooses from dropdown list
    When User clicks on button Add to Wishlist
    Then User refers to sign in page



