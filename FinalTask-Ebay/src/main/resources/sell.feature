Feature: TestToCreateAnItemToSell and TestToParticipateInAnAuction
  As a user
  I want to check ability of a user to participate in auction and create an item to sell without sign in
  So that I can see it works correctly

  Scenario: User creates to sell item without sign in
    Given User opens homepage page
    And User clicks on Sell
    And User clicks list an item
    And User enters brand name
    And User click Search
    When User click on view possible matches
    And User click on continue with selection
    And User selects condition
    And User press Next
    Then User refers to sign in page


  Scenario: User places a bid at Action without sign in
    Given User opens homepage page
    And User clicks on Shop By Category
    And User chooses category
    And User chooses MakeUp Product
    And User chooses Lips
    And User chooses Lips stain
    When User chooses Action
    And User chooses action to participate
    And User places a bid
    Then User refers to sign in page


