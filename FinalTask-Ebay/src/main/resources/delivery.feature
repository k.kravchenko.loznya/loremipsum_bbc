Feature: DeliveryTestSet
  As a user
  I want to check delivery options
  To confirm it works correctly

  Scenario: User checks delivery options
    Given User opens homepage page
    And User searches by new keyword
    And User clicks search button
    And User opens new product
    And User clicks on shipping details
    When User selects change country
    And User selects country from dropdown
    And User presses Get rated
    And User changes Quantity
    Then User presses Get rated



  Scenario: Check Ship to
    Given User opens homepage page
    And User clicks on Ship to
    And User clicks on Select
    When User choose country
  Then User press Done




