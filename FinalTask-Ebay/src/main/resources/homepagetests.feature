Feature: HomePageTest
  As a user
  I want to check some functionality on the HomePage
  To confirm it works correctly


  Scenario: Check Watchlist/Notification error message on the Home page without sign in
    Given User opens homepage page
    And User clicks on the Watchlist
    And Message pops up about Watchlist
    When User clicks on the Bell
    Then Message pops up about notifications


  Scenario: Check Cart/My Ebay error message on the Home page without sign in
    Given User opens homepage page
    And User clicks on the empty Cart
    And User gets message cart is empty
    When User clicks on My Ebay
    Then User refers to sign in page




