﻿
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Linq;


namespace Limsum
{
    public class Tests
    {
        private IWebDriver driver;
        static int CountLorem = 0;
        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver("C:\\Test\\ChromeDriver94");
            driver.Navigate().GoToUrl("https://lipsum.com/");
            driver.Manage().Window.Maximize();
        }

        [Test, Description("Task1/Part1-Test#1")]
        public void VerifyTheWordFishCorrectlyAppearsInThe1Paragraph()
        {
            driver.FindElement(By.XPath("//a[@href='http://ru.lipsum.com/']")).Click();
            var ElementTextFish = driver.FindElement(By.XPath("//*[contains(text(), 'рыба')]")).Text;
            Assert.True(ElementTextFish.Contains("рыба"));
        }
        [Test, Description("Task1/Part2-Test#2")]
        public void VerifyDefaultSettingResultStartingWithTextLoremIpsum()
        {
            driver.FindElement(By.XPath("//input [@name='generate']")).Click();
            string ElementTextFish = driver.FindElement(By.XPath("//div[@id = 'lipsum']/p[1]")).Text;
            Assert.True(ElementTextFish.Contains("Lorem ipsum dolor sit amet, consectetur adipiscing elit."));
        }
        [Test, Description("Task2/Part2-Test#1")]
        public void VerifyLoremIpsumIsGeneratedWithCorrectSize()
        {
            driver.FindElement(By.XPath("//input[@value='words']")).Click();
            driver.FindElement(By.XPath("//input[@name='amount']")).Clear();
            driver.FindElement(By.XPath("//input[@name='amount']")).SendKeys("10");
            driver.FindElement(By.XPath("//input [@name='generate']")).Click();
            string ElementTextGenerated = driver.FindElement(By.XPath("//div[@id = 'generated']")).Text;
            Assert.True(ElementTextGenerated.Contains("10 words"));
            string StringElement = driver.FindElement(By.XPath("//div[@id='lipsum']")).Text;
            int ActualAmountOfWords = StringElement.Split(" ").Length;
            Assert.AreEqual(ActualAmountOfWords, 10);
        }
        [Test, Description("Task2/Part2-Test#2")]
        public void VerifyTheCheckbox()
        {
            driver.FindElement(By.XPath("//input[@name='start']")).Click();
            driver.FindElement(By.XPath("//input [@name='generate']")).Click();
            string ElementText = driver.FindElement(By.XPath("//div[@id = 'lipsum']/p[1]")).Text;
            Assert.False(ElementText.StartsWith("Lorem ipsum"));
        }
        [Test, Description("Task2/Part2-Test#3"), Order(1), Category("Lorem verifications test suit")]
        [Repeat(10)]
        public void CheckIfRandomlyGeneratedTextContainLorem()
        {
            driver.FindElement(By.XPath("//input[@name='amount']")).Clear();
            driver.FindElement(By.XPath("//input[@name='amount']")).SendKeys("5");
            driver.FindElement(By.XPath("//input [@name='generate']")).Click();
            string ElementsText = driver.FindElement(By.XPath("//div[@id = 'lipsum']")).Text;
            string FindLorem = "lorem";
            ElementsText = ElementsText.Substring(0, ElementsText.Length);
            string[] Elements = ElementsText.Split(Convert.ToChar(" "));
            int Count = Elements.Where(s => s == FindLorem).Count();
            CountLorem += Count;
        }
        [Test, Description("Task2/Part2-Test#3"), Order(2), Category("Lorem verifications test suit")]
        public void CheckIfRandomlyGeneratedAfter10repeats()
        {
            int CountAverageLorem = CountLorem / 10;
            Assert.True(CountAverageLorem >= 2);
        }
        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}