package pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


import java.util.List;


public class SearchResultsPage extends BasePage {
    @FindBy(xpath = "//div[@class = 'prod-cart height']")
    private WebElement searchResult;

    @FindBy(xpath = "//div[@class = 'prod-status']")
    private WebElement productStatusText;

    @FindBy(xpath = "//div[@class= 'row js_more_content js_height-block']")
    private List<WebElement> listOfElements;



    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public void setSearchResult(){

        searchResult.click();
    }

    public String getText() {
        return productStatusText.getText();
    }

    public int getListSize() {
        return listOfElements.size();
    }

}
