package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultsPage extends BasePage{
    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }
    private static final long TIME_TO_WAIT = 100;

    @FindBy(xpath = "//img [@alt='[rom&nd] ROMAND Juicy Lasting Tint (20Colors)']")
    private WebElement romandJuicyLink;
    @FindBy(xpath = "//img[@alt='[ROM&ND] Romand Zero Velvet Tint 5.5g / 11 colors']")
    private WebElement romandZeroLink;
    @FindBy(xpath = "//img[@src='https://i.ebayimg.com/thumbs/images/g/WsMAAOSwXMxb0zGA/s-l225.webp']")
    private WebElement makeUpProductButton;
    @FindBy(xpath = "//img[@alt='Lips']")
    private WebElement lipsButton;
    @FindBy(xpath = "//img[@alt='Lip Stain']")
    private WebElement lipStainButton;
    @FindBy(xpath = "//li//h2[@class=\"srp-format-tabs-h2\"][contains(text(),'Auction')]")
    private WebElement auctionButton;
    @FindBy(xpath = "//img[@alt='Revlon Lip Balm Stain Honey Douce 001 Natural Pink Shade Discontinued New Sealed']")
    private WebElement revlonLink;

    public void chooseProduct() {
        waitVisibilityOfElement(TIME_TO_WAIT,romandJuicyLink);
        romandJuicyLink.click();
    }
    public void openProduct() {
        waitVisibilityOfElement(TIME_TO_WAIT,romandJuicyLink);
        romandJuicyLink.click();
    }
    public void chooseNewProduct() {
        waitVisibilityOfElement(TIME_TO_WAIT,romandZeroLink);
        romandZeroLink.click();
    }
    public void clickOnMakeUp(){
        makeUpProductButton.click();
    }
    public void clickOnLipStain(){
        lipStainButton.click();
    }
    public void clickOnLips(){
        lipsButton.click();
    }
    public void selectAction() {
        waitVisibilityOfElement(TIME_TO_WAIT, auctionButton);
        auctionButton.click();
    }
    public void clickOnRevlonAution(){
        waitVisibilityOfElement(TIME_TO_WAIT, revlonLink);
        revlonLink.click();
    }
}

