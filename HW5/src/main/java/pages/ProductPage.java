package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage{
    private static final String ERROR_MESSAGE_TEXT = "Please select a Color";

    @FindBy(xpath = "//a[@_sp='p2047675.l1356']")
    private WebElement buyItNowButton;
    @FindBy(xpath = "//div[@id='msku-sel-1-errMsg']")
    private WebElement errorMessage;
    @FindBy(xpath = "//a[@id='isCartBtn_btn']")
    private WebElement addToCartButton;
    @FindBy(xpath = "//div[@id='vi-atl-lnk']")
    private WebElement addToWishlistButton;
    @FindBy(xpath = "//select[@name = 'Color']")
    private WebElement selectDropDown;
    @FindBy(xpath = "//select[@name = 'Color']//option[7]")
    private WebElement colorButton;
    @FindBy(xpath = "//button[@id='sbin-gxo-btn']")
    private WebElement checkoutAsAGuestButton;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnBuyItNowButton() {
        buyItNowButton.click();
    }
//    public void getErrorMessage() {
//        assertTrue(ERROR_MESSAGE_TEXT,errorMessage.isDisplayed());
//    }
    public void clickAddToCartButton() { addToCartButton.click(); }
    public void clickAddToWishlistButton() {
        addToWishlistButton.click();
    }
    public void clickOnSelect()  {
        selectDropDown.click();
    }
    public void chooseFromDropDownList() {
        colorButton.click();
    }
    public void checkoutAsAGuest() { checkoutAsAGuestButton.click(); }


}
