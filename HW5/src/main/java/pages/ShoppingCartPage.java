package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.testng.Assert.assertEquals;


public class ShoppingCartPage extends BasePage {

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
    }
    private String numInTheCartExpected = "Shopping cart (1 item)";
    private String errorMessageTextEmptyCart ="You don't have any items in your cart.";
    private String firstName = "Valentina";
    private String lastName = "Valento";
    private String streetAddress = "St.18, Mechnikova";
    private String zipcode = "03568";
    private String email = "v.valento@gmail.com";
    private String phoneNum = "631234573";
    private String cityName = "Lviv";

    @FindBy(xpath ="//h1[@data-test-id='main-title']")
    private WebElement numberOfProductsInACart;
    @FindBy (xpath = "//button[@class='call-to-action btn btn--large btn--primary']")
    private WebElement checkoutButton;
    @FindBy (xpath = "//span[contains(text(), 'You don')]")
    private WebElement errorMessageForEmptyCart;
    @FindBy (xpath = "//select[@id = 'country']")
    private WebElement dropdownList;
    @FindBy (xpath = "//select[@id = 'country']/option UA")
    private WebElement countryName;
    @FindBy (xpath = "//input[@autocomplete='given-name']")
    private WebElement firstNameField;
    @FindBy (xpath = "//input[@autocomplete='family-name']")
    private WebElement lastNameField;
    @FindBy (xpath = "//input[@autocomplete='address-line1']")
    private WebElement addressField;
    @FindBy (xpath = "//input[@autocomplete='address-level2']")
    private WebElement cityField;
    @FindBy (xpath = "//input[@autocomplete='postal-code']")
    private WebElement zipcodeField;
    @FindBy (xpath = "//input[@name='email']")
    private WebElement emailField1;
    @FindBy (xpath = "//input[@name='emailConfirm']")
    private WebElement emailField2;
    @FindBy (xpath = "//button[@id ='phoneCountryCode']")
    private WebElement countryCodeField;
    @FindBy (xpath = "//input[@name ='phoneNumber']")
    private WebElement phoneNumField;
    @FindBy (xpath = "//button[@data-action-name='ADD_ADDRESS_SUBMIT']")
    private WebElement doneButton;

    public void getNumberInTheCart() {
        assertEquals(numInTheCartExpected,numberOfProductsInACart.getText()); }
    public void clickCheckoutButton(){
        checkoutButton.click();
    }
    public void getErrorMessageForEmptyCart() {
       errorMessageForEmptyCart.isDisplayed();
       assertEquals(errorMessageTextEmptyCart,errorMessageForEmptyCart.getText());
        }
    public void selectCountry(){dropdownList.click(); }
    public void chooseCountry(){countryName.click(); }
    public void setFirstName(){firstNameField.sendKeys(firstName); }
    public void setLastName(){lastNameField.sendKeys(lastName); }
    public void setStreetAddress(){addressField.sendKeys(streetAddress); }
    public void setCity(){cityField.sendKeys(cityName); }
    public void setZipcode(){zipcodeField.sendKeys(zipcode); }
    public void setEmail(){emailField1.sendKeys(email); }
    public void confirmEmail(){emailField2.sendKeys(email); }
    public void selectCountryCode(){countryCodeField.click(); }
    public void setPhoneNumber(){phoneNumField.sendKeys(phoneNum); }
    public void clickDone(){doneButton.click(); }
}
