package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;
import java.net.URL;


import pages.*;
import utils.CapabilityFactory;

public class BaseTest {

    protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();
    private CapabilityFactory capabilityFactory = new CapabilityFactory();

    private static final String EBAY_URL = "https://ebay.com";

    @BeforeMethod
    @Parameters(value = {"browser"})
    public void setUp(@Optional("firefox") String browser) throws MalformedURLException {
        driver.set(new RemoteWebDriver(new URL("http://192.168.1.37:4445/wd/hub"), capabilityFactory.getCapabilities(browser)));
        getDriver().manage().window().maximize();
        getDriver().get(EBAY_URL);
    }

    @AfterMethod
    public void tearDown() {
        getDriver().close();
    }

    @AfterClass
    void terminate() {
        driver.remove();
    }

    public WebDriver getDriver() {
        return driver.get();
    }

    public HomePage getHomePage() {
        return new HomePage(getDriver());
    }
    public ProductPage getProductPage() {
        return new ProductPage(getDriver());
    }
    public SellAnItemPage getSellAnItemPage() {
        return new SellAnItemPage(getDriver());
    }
    public ShoppingCartPage getShoppingCartPage() {
        return new ShoppingCartPage(getDriver());
    }
    public SignInPage getSignInPage() {
        return new SignInPage(getDriver());
    }
    public SearchResultsPage getSearchResultsPage() {
        return new SearchResultsPage(getDriver());
    }


}
