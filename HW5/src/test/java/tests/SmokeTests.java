package tests;


import org.testng.annotations.Test;

public class SmokeTests extends BaseTest{

    private static final long DEFAULT_WAITING_TIME = 100;

    @Test
    public void createToSellAnItemWithoutSignIn() {
        getHomePage().clickOnSell();
        getSellAnItemPage().clickOnListAnItemButton();
        getSellAnItemPage().enterBrandName();
        getSellAnItemPage().clickSearchButton();
        getSellAnItemPage().clickOnViewPossibleMatches();
        getSellAnItemPage().clickContinueWithSelectionButton();
        getSellAnItemPage().clickOnRadioButtonNew();
        getSellAnItemPage().clickNextButton();
        getSignInPage();

    }
    @Test
    public void checkCartAndMyEbay() {
    getHomePage().clickOnTheCartButton();
  getShoppingCartPage().getErrorMessageForEmptyCart();
  getHomePage().clickOnMenuMyEbay();
  getSignInPage();
    }
    @Test
    public void checkBuyNowWithoutSignIn () {
        getHomePage().searchForProduct();
        getHomePage().clickSearchButton();
        getSearchResultsPage().chooseProduct();
        getProductPage().clickOnSelect();
        getProductPage().chooseFromDropDownList();
        getProductPage().clickOnBuyItNowButton();
        getProductPage().checkoutAsAGuest();
    }
    @Test
    public void checkAddToCartWithoutSignIn () {
        getHomePage().searchForProduct();
        getHomePage().clickSearchButton();
        getSearchResultsPage().chooseProduct();
        getProductPage().clickOnSelect();
        getProductPage().chooseFromDropDownList();
        getProductPage().clickAddToCartButton();
        getShoppingCartPage().getNumberInTheCart();
        getShoppingCartPage().clickCheckoutButton();
        getSignInPage();

    }
}
