Feature: TestBBCOne
  As a user
  I want to check functionality of a site
  To confirm it works correctly

  Scenario: User checks the title of the headline article
    Given User opens homepage page BBC
    When User clicks on News tab
    Then User checks that title of the main article is the same as expected

  Scenario: User checks the title of the five other article in main page
    Given User opens homepage page BBC
    When User clicks on News tab
    And User checks that title of the second article is the same as expected
    And User checks that title of the third article is the same as expected
    And User checks that title of the fourth article is the same as expected
    And User checks that title of the fifth article is the same as expected
    Then User checks that title of the sixth article is the same as expected

  Scenario: User enters category link to search bar and checks first article title
    Given User opens homepage page BBC
    When User clicks on News tab
    And User gets and search Category name
    Then User check if it has the same value as expected

  Scenario: User checks form to ask your question without clicking on checkbox
    Given User opens homepage page BBC
    When User clicks on News tab
    And User goes to the Question Form
    And User fills question field
    And User fills name field
    And User fills email field
    And User fills number field
    And User fills location field
    And User pushes submit button
    Then User checks error message without checking checkbox


  Scenario: User checks form to ask your question without question
    Given User opens homepage page BBC
    When User clicks on News tab
    And User goes to the Question Form
    And User fills name field
    And User fills email field
    And User fills number field
    And User fills location field
    And User click on checkbox
    And User pushes submit button
    Then User checks error message without a question

  Scenario: User checks form to ask your question without a name
    Given User opens homepage page BBC
    When User clicks on News tab
    And User goes to the Question Form
    And User fills question field
    And User fills email field
    And User fills number field
    And User fills location field
    And User click on checkbox
    And User pushes submit button
    Then User checks error message without a name

  Scenario: User checks form to ask your question without email
    Given User opens homepage page BBC
    When User clicks on News tab
    And User goes to the Question Form
    And User fills question field
    And User fills email field
    And User fills number field
    And User fills location field
    And User click on checkbox
    And User pushes submit button
    Then User checks error message without email
