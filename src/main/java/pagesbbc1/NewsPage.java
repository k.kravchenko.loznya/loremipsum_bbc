package pagesbbc1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import static org.openqa.selenium.By.xpath;

public class NewsPage extends BasePage{
    public NewsPage(WebDriver driver) {
        super(driver);
    }
    private static final String TITLE_OF_THE_HEADLINE_ARTICLE = "//div[@data-entityid='container-top-stories#1']";
    private static final String TITLE_OF_THE_ARTICLE_2 ="//div[@data-entityid='container-top-stories#2']";
    private static final String TITLE_OF_THE_ARTICLE_3 ="//div[@data-entityid='container-top-stories#3']";
    private static final String TITLE_OF_THE_ARTICLE_4 ="//div[@data-entityid='container-top-stories#4']";
    private static final String TITLE_OF_THE_ARTICLE_5 ="//div[@data-entityid='container-top-stories#5']";
    private static final String TITLE_OF_THE_ARTICLE_6 ="//div[@data-entityid='container-top-stories#6']";
    private static final String STORIES_TAB="//ul [@class='gs-o-list-ui--top-no-border nw-c-nav__wide-sections']//a[@href='/news/stories']";
    private static final String SEARCH_FIELD="//input[@id='orb-search-q']";
    private static final String SEARCH_BUTTON="//button[@id='orb-search-button']";
    private static final String TITLE_OF_THE_FIRST_ARTICLE_IN_STORIES = "(//a[@class='ssrcss-bxvqns-PromoLink e1f5wbog0'])[1]";
    private static final String CORONAVIRUS_TAB="//ul [@class='gs-o-list-ui--top-no-border nw-c-nav__wide-sections']//a[@href='/news/coronavirus']";
    private static final String YOUR_CORONAVIRUS_STORIES_TAB="//div[@class='gs-u-display-none gs-u-display-block@m']//a [@href='/news/have_your_say']";
    private static final String SEND_US_QUESTION_FORM="//a[@href='/news/52143212']";


    @FindBy(xpath = "//div[@data-entityid='container-top-stories#1']")
    private WebElement titleOfAHeadlineArticle;
    @FindBy(xpath ="//div[@data-entityid='container-top-stories#2']")
    private  WebElement titleOfASecondArticle;
    @FindBy(xpath ="//div[@data-entityid='container-top-stories#3']")
    private  WebElement titleOfAThirdArticle;
    @FindBy(xpath ="//div[@data-entityid='container-top-stories#4']")
    private  WebElement titleOfAFourthArticle;
    @FindBy(xpath ="//div[@data-entityid='container-top-stories#5']")
    private  WebElement titleOfAFifthArticle;
    @FindBy(xpath ="//div[@data-entityid='container-top-stories#6']")
    private  WebElement titleOfASixthArticle;
    @FindBy(xpath = "//ul [@class='gs-o-list-ui--top-no-border nw-c-nav__wide-sections']//a[@href='/news/stories']")
    private  WebElement storiesTab;
    @FindBy(xpath ="//input[@id='orb-search-q']")
    private  WebElement searchField;
    @FindBy(xpath ="//button[@id='orb-search-button']")
    private  WebElement searchButton;
    @FindBy(xpath ="(//a[@class='ssrcss-bxvqns-PromoLink e1f5wbog0'])[1]")
    private  WebElement titleOfAFirstArticleInStories;
    @FindBy(xpath ="//ul [@class='gs-o-list-ui--top-no-border nw-c-nav__wide-sections']//a[@href='/news/coronavirus']")
    private  WebElement coronavirusTab;
    @FindBy(xpath ="//div[@class='gs-u-display-none gs-u-display-block@m']//a [@href='/news/have_your_say']")
    private  WebElement yourCoronavirusStories;
    @FindBy(xpath ="//a[@href='/news/52143212']")
    private  WebElement sendUsQuestionForm;

    private static final String WORD_EXPECTED_IN_HEADLINE_ARTICLE = "China";
    private static final String WORD_EXPECTED_ARTICLE2 = "Austrian";
    private static final String WORD_EXPECTED_ARTICLE3 = "Americans";
    private static final String WORD_EXPECTED_ARTICLE4 = "Top";
    private static final String WORD_EXPECTED_ARTICLE5 = "Fury";
    private static final String WORD_EXPECTED_ARTICLE6 = "84";
    private static final String WORD_EXPECTED_VALUE =  "Stories";
    private static final long TIME_TO_WAIT =100;

    public String getTextHeadlineArticleTitle() {
        return driver.findElement(xpath(TITLE_OF_THE_HEADLINE_ARTICLE)).getText(); }
    public String getTextArticleTwoTitle() {
    return driver.findElement(xpath(TITLE_OF_THE_ARTICLE_2)).getText();}
    public String getTextArticleThreeTitle() {
        return driver.findElement(xpath(TITLE_OF_THE_ARTICLE_3)).getText();}
    public String getTextArticleFourTitle() {
        return driver.findElement(xpath(TITLE_OF_THE_ARTICLE_4)).getText();}
    public String getTextArticleFiveTitle() {
        return driver.findElement(xpath(TITLE_OF_THE_ARTICLE_5)).getText();}
    public String getTextArticleSixTitle() {
        return driver.findElement(xpath(TITLE_OF_THE_ARTICLE_6)).getText();}
    public void getAndSearchCategoryName() {
        String elementText = driver.findElement(xpath(STORIES_TAB)).getText();
        driver.findElement(xpath(SEARCH_FIELD)).sendKeys(elementText);
        driver.findElement(xpath(SEARCH_BUTTON)).click();}
    public String getTextValueOfTheFirstArticle(){
    return driver.findElement(xpath(TITLE_OF_THE_FIRST_ARTICLE_IN_STORIES)).getText();}
    public void getToTheQuestionsForm(){
    driver.findElement(xpath(CORONAVIRUS_TAB)).click();
        driver.findElement(xpath(YOUR_CORONAVIRUS_STORIES_TAB)).click();
        driver.findElement(xpath(SEND_US_QUESTION_FORM)).click();}


    public void checkTextHeadlineArticleTitleIsAsExpectedCuc() {
        waitForPageLoadComplete(TIME_TO_WAIT);
        waitVisibilityOfElement(TIME_TO_WAIT,titleOfAHeadlineArticle);
        String elementText =titleOfAHeadlineArticle.getText();
        Assert.assertTrue(elementText.contains(WORD_EXPECTED_IN_HEADLINE_ARTICLE)); }
    public void checkTextArticleSecondTitleIsTheSameAsExpectedCuc() {
        waitVisibilityOfElement(TIME_TO_WAIT, titleOfASecondArticle);
        String elementText =titleOfASecondArticle.getText();
        Assert.assertTrue(elementText.contains(WORD_EXPECTED_ARTICLE2)); }
    public void checkTextArticleThirdTitleIsTheSameAsExpectedCuc() {
        waitVisibilityOfElement(TIME_TO_WAIT, titleOfAThirdArticle);
        String elementText =titleOfAThirdArticle.getText();
        Assert.assertTrue(elementText.contains(WORD_EXPECTED_ARTICLE3)); }
    public void checkTextArticleFourthTitleIsTheSameAsExpectedCuc() {
        waitVisibilityOfElement(TIME_TO_WAIT, titleOfAFourthArticle);
        String elementText =titleOfAFourthArticle.getText();
        Assert.assertTrue(elementText.contains(WORD_EXPECTED_ARTICLE4)); }
    public void checkTextArticleFifthTitleIsTheSameAsExpectedCuc() {
        waitVisibilityOfElement(TIME_TO_WAIT, titleOfAFifthArticle);
        String elementText =titleOfAFifthArticle.getText();
        Assert.assertTrue(elementText.contains(WORD_EXPECTED_ARTICLE5)); }
    public void checkTextArticleSixthTitleIsTheSameAsExpectedCuc() {
        waitVisibilityOfElement(TIME_TO_WAIT, titleOfASixthArticle);
        String elementText =titleOfASixthArticle.getText();
        Assert.assertTrue(elementText.contains(WORD_EXPECTED_ARTICLE6)); }
    public void getAndSearchCategoryNameCuc() {
        String elementText = storiesTab.getText();
        searchField.sendKeys(elementText);
        searchButton.click();}
    public void checkTextValueOfTheFirstArticleCuc(){
        String elementText1 = titleOfAFirstArticleInStories.getText();
        Assert.assertTrue(elementText1.contains(WORD_EXPECTED_VALUE));}
    public void getToTheQuestionsFormCuc(){
        coronavirusTab.click();
        yourCoronavirusStories.click();
        sendUsQuestionForm.click();}

}
