package pagesbbc1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;



import static org.openqa.selenium.By.xpath;

public class HomePage extends  BasePage{
    public HomePage(WebDriver driver) {
        super(driver);
    }
    private static final String NEWS_LINK = "//div[@id='orb-nav-links']//a [@href='https://www.bbc.com/news']";
    private static final String BBC1_URL ="https://www.bbc.co.uk/bbcone";
    @FindBy(xpath = "//div[@id='orb-nav-links']//a [@href='https://www.bbc.com/news']")
    private WebElement newsLink;
    private static final long TIME_TO_WAIT = 100;
    public void clickOnNews () {
        driver.findElement(xpath(NEWS_LINK)).click();
    }

    public void clickOnNewsCuc(){
        waitVisibilityOfElement(TIME_TO_WAIT,newsLink);
        newsLink.click(); }
    public void openHomePage(){driver.get(BBC1_URL);
    waitForPageLoadComplete(TIME_TO_WAIT);}
}
