package pagesbbc1;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import static java.lang.Thread.sleep;
import static org.openqa.selenium.By.xpath;

public class QuestionFormPage extends BasePage{
    public QuestionFormPage(WebDriver driver) {
        super(driver);
    }
    private static final String QUESTION_FIELD="//textarea[@class='text-input--long']";
    private static final String NAME_FIELD="//input[@placeholder='Name']";
    private static final String EMAIL_FIELD= "//input[@placeholder='Email address']";
    private static final String PHONE_NUMBER_FIELD = "//input[@placeholder='Contact number']";
    private static final String LOCATION_FIELD = "//input[@placeholder='Location ']";
    private static final String SUBMIT_BUTTON ="//button[@class='button']";
    private static final String ERROR_MESSAGE_WITHOUT_CHECK_THE_CHECKBOX ="//div[contains (text(), 'accepted')]";
    private static final String CHECKBOX = "//input[@type='checkbox']";
    private static final String ERROR_MESSAGE_WITHOUT_A_QUESTION= "//div[contains (text(), 'blank')]";
    private static final String ERROR_MESSAGE_WITHOUT_A_NAME="//div[contains (text(), 'Name')]";
    private static final String ERROR_MESSAGE_WITHOUT_A_EMAIL="//div[contains (text(), 'Email')]";


    @FindBy(xpath ="//button[@class='button']")
            private WebElement submitButton;
    @FindBy (xpath ="//div[contains (text(), 'accepted')]")
            private WebElement errorMessageWithoutCheckingCheckBox;
    @FindBy (xpath = "//input[@type='checkbox']")
            private WebElement checkbox;
    @FindBy (xpath = "//div[contains (text(), 'blank')]")
            private WebElement errorMessageWithoutQuestion;
    @FindBy (xpath  ="//div[contains (text(), 'Name')]")
            private WebElement errorMessageWithoutName;
    @FindBy (xpath  ="//div[contains (text(), 'Email')]")
            private WebElement errorMessageWithoutEmail;

    @FindBy (xpath ="//textarea[@class='text-input--long']")
    private WebElement questionField;
    @FindBy (xpath ="//input[@placeholder='Name']")
    private WebElement nameField;
    @FindBy (xpath = "//input[@placeholder='Email address']")
    private WebElement emailField;
    @FindBy (xpath  = "//input[@placeholder='Contact number']")
    private WebElement phoneNumberField;
    @FindBy (xpath  = "//input[@placeholder='Location ']")
    private WebElement locationField;

    private static final String ERROR_EXPECTED_MESSAGE_FOR_UNCHECK_CHECKBOX =  "must be accepted";
    private static final String ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_QUESTION ="can't be blank";
    private static final String ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_NAME ="Name can't be blank";
    private static final String ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_EMAIL ="Email address can't be blank";
    private static final long TIME_TO_WAIT =100;
    private static final long TIME_TO_WAIT_FOR_SLEEP =300;

    public void enterQuestion(String string){
    driver.findElement(xpath(QUESTION_FIELD)).sendKeys(string);}
    public void enterName(String string){
    driver.findElement(xpath(NAME_FIELD)).sendKeys(string);}
    public void enterEmail(String string){
        driver.findElement(xpath(EMAIL_FIELD)).sendKeys(string);}
    public void enterPhoneNumber(String string){
        driver.findElement(xpath(PHONE_NUMBER_FIELD)).sendKeys(string);}
    public void enterLocation(String string){
        driver.findElement(xpath(LOCATION_FIELD)).sendKeys(string);}
    public void pressSubmitButton() throws InterruptedException {
      sleep(TIME_TO_WAIT_FOR_SLEEP);
        driver.findElement(xpath(SUBMIT_BUTTON)).click();}
    public String getErrorMessageWithoutCheckTheCheckbox() throws InterruptedException {
    waitForPageLoadComplete(TIME_TO_WAIT);
        sleep(TIME_TO_WAIT_FOR_SLEEP);
    return driver.findElement(xpath(ERROR_MESSAGE_WITHOUT_CHECK_THE_CHECKBOX)).getText();}

    public void clickOnCheckbox() throws InterruptedException {
    sleep(TIME_TO_WAIT_FOR_SLEEP);
    Object element = driver.findElement(xpath(CHECKBOX));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();",element); }
    public String getErrorMessageWithoutAQuestion() throws InterruptedException {
        waitForPageLoadComplete(TIME_TO_WAIT);
        sleep(TIME_TO_WAIT_FOR_SLEEP);
    return driver.findElement(xpath(ERROR_MESSAGE_WITHOUT_A_QUESTION)).getText();}
    public String getErrorMessageWithoutAName() throws InterruptedException {
        waitForPageLoadComplete(TIME_TO_WAIT);
        sleep(TIME_TO_WAIT_FOR_SLEEP);
        return driver.findElement(xpath(ERROR_MESSAGE_WITHOUT_A_NAME)).getText();}
    public String getErrorMessageWithoutAEmail() throws InterruptedException {
        waitForPageLoadComplete(TIME_TO_WAIT);
        sleep(TIME_TO_WAIT_FOR_SLEEP);
        return driver.findElement(xpath(ERROR_MESSAGE_WITHOUT_A_EMAIL)).getText();}




    public void pressSubmitButtonCuc() throws InterruptedException {
        //sleep(300);
        waitVisibilityOfElement(TIME_TO_WAIT_FOR_SLEEP, submitButton);
        submitButton.click();}
    public void getErrorMessageWithoutCheckTheCheckboxCuc() throws InterruptedException {
        waitForPageLoadComplete(TIME_TO_WAIT);
        //sleep(300);
        waitVisibilityOfElement(TIME_TO_WAIT_FOR_SLEEP, errorMessageWithoutCheckingCheckBox);
        String elementText = errorMessageWithoutCheckingCheckBox.getText();
        Assert.assertEquals(elementText, ERROR_EXPECTED_MESSAGE_FOR_UNCHECK_CHECKBOX);}

    public void clickOnCheckboxCuc() throws InterruptedException {
        //sleep(300);
        waitVisibilityOfElement(TIME_TO_WAIT_FOR_SLEEP, checkbox);
        Object element = checkbox;
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();",element); }
    public void getErrorMessageWithoutAQuestionCuc() throws InterruptedException {
        waitForPageLoadComplete(TIME_TO_WAIT);
        //sleep(300);
        waitVisibilityOfElement(TIME_TO_WAIT_FOR_SLEEP, errorMessageWithoutQuestion);
        String elementText = errorMessageWithoutQuestion.getText();
        Assert.assertEquals(elementText, ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_QUESTION);
    }
    public void getErrorMessageWithoutANameCuc() throws InterruptedException {
        waitForPageLoadComplete(TIME_TO_WAIT);
        //sleep(300);
        waitVisibilityOfElement(TIME_TO_WAIT_FOR_SLEEP, errorMessageWithoutName);

        String elementText = errorMessageWithoutName.getText();
        Assert.assertEquals(elementText, ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_NAME);
    }
    public void getErrorMessageWithoutAEmailCuc() throws InterruptedException {
        waitForPageLoadComplete(TIME_TO_WAIT);
        //sleep(300);
        waitVisibilityOfElement(TIME_TO_WAIT_FOR_SLEEP, errorMessageWithoutEmail);
        String elementText =errorMessageWithoutEmail.getText();
        Assert.assertEquals(elementText, ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_EMAIL);
    }

        public void enterQuestionCuc(String string){
        questionField.sendKeys(string);}
        public void enterNameCuc(String string){
        nameField.sendKeys(string);}
        public void enterEmailCuc(String string){
        emailField.sendKeys(string);}
       public void enterPhoneNumberCuc(String string){
        phoneNumberField.sendKeys(string);}
        public void enterLocationCuc(String string){
        locationField.sendKeys(string);}
}
