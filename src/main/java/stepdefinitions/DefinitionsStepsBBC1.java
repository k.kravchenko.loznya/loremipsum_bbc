package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManagerBBC;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import pagesbbc1.Form;
import pagesbbc1.HomePage;
import pagesbbc1.NewsPage;
import pagesbbc1.QuestionFormPage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class DefinitionsStepsBBC1 {
    WebDriver driver;
    PageFactoryManagerBBC pageFactoryManagerBBC;
    HomePage homePage;
    Form form;
    NewsPage newsPage;
    QuestionFormPage questionFormPage;
    private static final int TIME_TO_WAIT = 60;
    private static final String QUESTION_FOR_FORM =  "Which one vaccine is better?";
    private static final String NAME_FOR_FORM =  "Kate";
    private static final String EMAIL_FOR_FORM =  "kdml";
    private static final String NUMBER_FOR_FORM =  "80501233445";
    private static final String LOCATION_FOR_FORM =  "Ukraine";



    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManagerBBC = new PageFactoryManagerBBC(driver);
    }
    @BeforeMethod
    public void testsSetup(){
        driver =  new ChromeDriver();
        driver.manage().window().maximize();
    }
    @After
    public void tearDown() {
        driver.close();
    }

    @Given("User opens homepage page BBC")
    public void openPageBBCOne() {
        homePage = pageFactoryManagerBBC.getHomePage();
        homePage.openHomePage();
        homePage.waitForPageLoadComplete(TIME_TO_WAIT);
    }

    @When("User clicks on News tab")
    public void userClicksOnNewsTab() {
        homePage.clickOnNewsCuc();
    }

    @Then("User checks that title of the main article is the same as expected")
    public void userChecksThatTitleOfTheMainArticleIsTheSameAsExpected() {
        newsPage=pageFactoryManagerBBC.getNewsPage();
        newsPage.checkTextHeadlineArticleTitleIsAsExpectedCuc();

    }

    @And("User checks that title of the second article is the same as expected")
    public void userChecksThatTitleOfTheSecondArticleIsTheSameAsExpected() {
        newsPage=pageFactoryManagerBBC.getNewsPage();
        newsPage.checkTextArticleSecondTitleIsTheSameAsExpectedCuc();
    }

    @And("User checks that title of the third article is the same as expected")
    public void userChecksThatTitleOfTheThirdArticleIsTheSameAsExpected() {
        newsPage.checkTextArticleThirdTitleIsTheSameAsExpectedCuc();
    }

    @And("User checks that title of the fourth article is the same as expected")
    public void userChecksThatTitleOfTheFourthArticleIsTheSameAsExpected() {
        newsPage.checkTextArticleFourthTitleIsTheSameAsExpectedCuc();
    }

    @And("User checks that title of the fifth article is the same as expected")
    public void userChecksThatTitleOfTheFifthArticleIsTheSameAsExpected() {
        newsPage.checkTextArticleFifthTitleIsTheSameAsExpectedCuc();
    }

    @Then("User checks that title of the sixth article is the same as expected")
    public void userChecksThatTitleOfTheSixthArticleIsTheSameAsExpected() {
        newsPage.checkTextArticleSixthTitleIsTheSameAsExpectedCuc();
    }

    @And("User gets and search Category name")
    public void userGetsAndSearchCategoryName() {
        newsPage=pageFactoryManagerBBC.getNewsPage();
        newsPage.getAndSearchCategoryNameCuc();
    }

    @Then("User check if it has the same value as expected")
    public void userCheckIfItHasTheSameValueAsExpected() {
        newsPage.checkTextValueOfTheFirstArticleCuc();
    }


    @And("User goes to the Question Form")
    public void userGoesToTeQuestionForm() {
        newsPage = pageFactoryManagerBBC.getNewsPage();
        newsPage.getToTheQuestionsFormCuc();
    }

    @And("User fills question field")
    public void userFillsQuestionField() {
        questionFormPage = pageFactoryManagerBBC.getQuestionFormPage();
        questionFormPage.enterQuestionCuc(QUESTION_FOR_FORM);
    }

    @And("User fills name field")
    public void userFillsNameField() {
        questionFormPage = pageFactoryManagerBBC.getQuestionFormPage();
        questionFormPage.enterNameCuc(NAME_FOR_FORM);
    }

    @And("User fills email field")
    public void userFillsEmailField() {
        questionFormPage.enterEmailCuc(EMAIL_FOR_FORM);
    }

    @And("User fills number field")
    public void userFillsNumberField() {
        questionFormPage.enterPhoneNumberCuc(NUMBER_FOR_FORM);
    }

    @And("User fills location field")
    public void userFillsLocationField() {
        questionFormPage.enterLocationCuc(LOCATION_FOR_FORM);
    }

    @And("User pushes submit button")
    public void userPushesSubmitButton() throws InterruptedException {
        questionFormPage.pressSubmitButtonCuc();
    }

    @Then("User checks error message without checking checkbox")
    public void userChecksErrorMessageWithoutCheckingCheckbox() throws InterruptedException {
        questionFormPage.getErrorMessageWithoutCheckTheCheckboxCuc();
    }

    @And("User click on checkbox")
    public void userClickOnCheckbox() throws InterruptedException {
        questionFormPage.clickOnCheckboxCuc();
    }

    @Then("User checks error message without a question")
    public void userChecksErrorMessageWithoutAQuestion() throws InterruptedException {
        questionFormPage.getErrorMessageWithoutAQuestionCuc();
    }

    @Then("User checks error message without a name")
    public void userChecksErrorMessageWithoutAName() throws InterruptedException {
        questionFormPage.getErrorMessageWithoutANameCuc();
    }

    @Then("User checks error message without email")
    public void userChecksErrorMessageWithoutEmail() throws InterruptedException {
        questionFormPage.getErrorMessageWithoutAEmailCuc();
    }
}
