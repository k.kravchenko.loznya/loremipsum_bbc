package manager;

import org.openqa.selenium.WebDriver;
import pagesbbc1.Form;
import pagesbbc1.HomePage;
import pagesbbc1.NewsPage;
import pagesbbc1.QuestionFormPage;


public class PageFactoryManagerBBC {
    WebDriver driver;
    public PageFactoryManagerBBC(WebDriver driver) {
        this.driver = driver;
    }


   public HomePage getHomePage(){return new HomePage(driver);}
   public QuestionFormPage getQuestionFormPage(){return new QuestionFormPage(driver);}
   public Form getForm(){return new Form(driver);}
   public NewsPage getNewsPage(){return new NewsPage(driver);}

}

