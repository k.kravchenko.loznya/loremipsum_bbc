package bbc1test;


import org.testng.Assert;
import org.testng.annotations.Test;


public class BBCTest extends BaseTest{
    private static final String WORD_EXPECTED_IN_HEADLINE_ARTICLE = "China";
    private static final String WORD_EXPECTED_ARTICLE_2 = "science";
    private static final String WORD_EXPECTED_ARTICLE_3 = "new";
    private static final String WORD_EXPECTED_ARTICLE_4 = "carpet";
    private static final String WORD_EXPECTED_ARTICLE_5 = "Nobel";
    private static final String WORD_EXPECTED_ARTICLE_6 = "Prince";
    private static final String WORD_EXPECTED_VALUE =  "Stories";
    private static final String QUESTION_FOR_FORM =  "Which one vaccine is better?";
    private static final String QUESTION_FOR_FORM_3 =  "How often do I need to be vaccinated?";
    private static final String QUESTION_FOR_FORM_4 =  "Where can see information about current amount of people vaccinated in other countries?";
    private static final String NAME_FOR_FORM =  "Kate";
    private static final String NAME_FOR_FORM_2 =  "Kamila";
    private static final String NAME_FOR_FORM_4 =  "Kaara";
    private static final String EMAIL_FOR_FORM =  "kdml";
    private static final String EMAIL_FOR_FORM_2 =  "kaaamila";
    private static final String EMAIL_FOR_FORM_3 =  "ka12v@m";
    private static final String NUMBER_FOR_FORM =  "80501233445";
    private static final String NUMBER_FOR_FORM_2 =  "80546712578";
    private static final String NUMBER_FOR_FORM_4 =  "805fssvm18";
    private static final String LOCATION_FOR_FORM =  "Ukraine";
    private static final String LOCATION_FOR_FORM_2 =  "UK";
    private static final String LOCATION_FOR_FORM_4 =  "Germany";
    private static final String ERROR_EXPECTED_MESSAGE_FOR_UNCHECK_CHECKBOX =  "must be accepted";
    private static final String ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_QUESTION ="can't be blank";
    private static final String ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_NAME ="Name can't be blank";
    private static final String ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_EMAIL ="Email address can't be blank";





    @Test(description = "Task1/Part1-Test#1")
    public void checksTheNameOfTheHeadlineArticle () {
        homePage().clickOnNews();
        String elementText = newsPage().getTextHeadlineArticleTitle();
        Assert.assertTrue(elementText.contains(WORD_EXPECTED_IN_HEADLINE_ARTICLE));
    }
    @Test(description = "Task1/Part1-Test#2")
    public void checksSecondaryArticleTitles () {
        homePage().clickOnNews();
        String elementText = newsPage().getTextArticleTwoTitle();
        Assert.assertTrue(elementText.contains(WORD_EXPECTED_ARTICLE_2));
        String elementText2 = newsPage().getTextArticleThreeTitle();
        Assert.assertTrue(elementText2.contains(WORD_EXPECTED_ARTICLE_3));
        String elementText3 = newsPage().getTextArticleFourTitle();
        Assert.assertTrue(elementText3.contains(WORD_EXPECTED_ARTICLE_4));
        String elementText4 = newsPage().getTextArticleFiveTitle();
        Assert.assertTrue(elementText4.contains(WORD_EXPECTED_ARTICLE_5));
        String elementText5 = newsPage().getTextArticleSixTitle();
        Assert.assertTrue(elementText5.contains(WORD_EXPECTED_ARTICLE_6));
    }
    @Test(description = "Task1/Part1-Test#3")
    public void inputCategoryLinkToSearchBarAndCheckFirstArticleName () {
        homePage().clickOnNews();
        newsPage().getAndSearchCategoryName();
        String elementText1 = newsPage().getTextValueOfTheFirstArticle();
        Assert.assertTrue(elementText1.contains(WORD_EXPECTED_VALUE));}

    @Test(description = "Task2/Part2-Test#1")
    public void checkFormToAskYourQuestionWithoutClickOnCheckbox() throws InterruptedException {
        homePage().clickOnNews();
        newsPage().getToTheQuestionsForm();
        questionFormPage().enterQuestion(QUESTION_FOR_FORM);
        questionFormPage().enterName(NAME_FOR_FORM);
        questionFormPage().enterEmail(EMAIL_FOR_FORM);
        questionFormPage().enterPhoneNumber(NUMBER_FOR_FORM);
        questionFormPage().enterLocation(LOCATION_FOR_FORM);
        questionFormPage().pressSubmitButton();
        String elementText = questionFormPage().getErrorMessageWithoutCheckTheCheckbox();
        Assert.assertEquals(elementText, ERROR_EXPECTED_MESSAGE_FOR_UNCHECK_CHECKBOX);
    }
    @Test(description = "Task2/Part2-Test#2")
    public void checkFormToAskYourQuestionWithoutQuestion() throws InterruptedException {
        homePage().clickOnNews();
        newsPage().getToTheQuestionsForm();
        questionFormPage().enterName(NAME_FOR_FORM_2);
        questionFormPage().enterEmail(EMAIL_FOR_FORM_2);
        questionFormPage().enterPhoneNumber(NUMBER_FOR_FORM_2);
        questionFormPage().enterLocation(LOCATION_FOR_FORM_2);
        questionFormPage().clickOnCheckbox();
        questionFormPage().pressSubmitButton();
        String elementText = questionFormPage().getErrorMessageWithoutAQuestion();
        Assert.assertEquals(elementText, ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_QUESTION);}

    @Test(description = "Task2/Part2-Test#3")
    public void checkFormToAskYourQuestionWithoutName () throws InterruptedException {
        homePage().clickOnNews();
        newsPage().getToTheQuestionsForm();
        questionFormPage().enterQuestion(QUESTION_FOR_FORM_3);
        questionFormPage().enterEmail(EMAIL_FOR_FORM_3);
        questionFormPage().clickOnCheckbox();
        questionFormPage().pressSubmitButton();
        String elementText = questionFormPage().getErrorMessageWithoutAName();
        Assert.assertEquals(elementText, ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_NAME);
    }
    @Test(description = "Task2/Part2-Test#4")
    public void checkFormToAskYourQuestionWithoutEmail() throws InterruptedException {
        homePage().clickOnNews();
        newsPage().getToTheQuestionsForm();
        questionFormPage().enterQuestion(QUESTION_FOR_FORM_4);
        questionFormPage().enterName(NAME_FOR_FORM_4);
        questionFormPage().enterPhoneNumber(NUMBER_FOR_FORM_4);
        questionFormPage().enterLocation(LOCATION_FOR_FORM_4);
        questionFormPage().clickOnCheckbox();
        questionFormPage().pressSubmitButton();
        String elementText1 = questionFormPage().getErrorMessageWithoutAEmail();
        Assert.assertEquals(elementText1, ERROR_EXPECTED_MESSAGE_FOR_SUBMITTING_WITHOUT_A_EMAIL);
    }


}