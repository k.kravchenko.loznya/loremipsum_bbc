package bbc1test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pagesbbc1.NewsPage;
import pagesbbc1.HomePage;
import pagesbbc1.QuestionFormPage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class BaseTest {
    private WebDriver driver;
    private static final String BBC1_URL ="https://www.bbc.co.uk/bbcone";

    @BeforeTest
    public void profileSetup(){

        chromedriver().setup();
    }

    @BeforeMethod
    public void testsSetup(){
        driver =  new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(BBC1_URL);
    }

    @AfterMethod
    public void tearDown() {

        driver.close();
    }

    public WebDriver getDriver(){
        return driver;
    }
    public HomePage homePage(){return new HomePage(getDriver()); }
    public NewsPage newsPage(){return new NewsPage(getDriver());}
    public QuestionFormPage questionFormPage(){return new QuestionFormPage(getDriver());}


}
