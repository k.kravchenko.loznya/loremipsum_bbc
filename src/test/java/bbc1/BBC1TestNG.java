package bbc1;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static java.lang.Thread.sleep;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.openqa.selenium.By.xpath;

public class BBC1TestNG {
    private WebDriver driver;


    @BeforeTest
    public void profileSetUp(){
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
    }
    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.bbc.co.uk/bbcone");
    }
    @AfterMethod
    public void tearDown() {
        driver.close();
    }

    @Test(description = "Task1/Part1-Test#1")
    public void checksTheNameOfTheHeadlineArticle () {
        driver.findElement(xpath("//div[@id='orb-nav-links']//a [@href='https://www.bbc.com/news']")).click();
        String elementText = driver.findElement(xpath("//div[@data-entityid='container-top-stories#1']")).getText();
        Assert.assertTrue(elementText.contains("Distraught families wait after Ecuador prison riot"));
    }
    @Test(description = "Task1/Part1-Test#2")
    public void checksSecondaryArticleTitles () {
        driver.findElement(xpath("//div[@id='orb-nav-links']//a [@href='https://www.bbc.com/news']")).click();
        String elementText = driver.findElement(xpath("//div[@data-entityid='container-top-stories#2']")).getText();
        Assert.assertTrue(elementText.contains("Sarah Everard's"));
        String elementText2 = driver.findElement(xpath("//div[@data-entityid='container-top-stories#3']")).getText();
        Assert.assertTrue(elementText2.contains("Nazi camp"));
        String elementText3 = driver.findElement(xpath("//div[@data-entityid='container-top-stories#4']")).getText();
        Assert.assertTrue(elementText3.contains("Youth have right"));
        String elementText4 = driver.findElement(xpath("//div[@data-entityid='container-top-stories#5']")).getText();
        Assert.assertTrue(elementText4.contains("Sarkozy gets jail"));
        String elementText5 = driver.findElement(xpath("//div[@data-entityid='container-top-stories#6']")).getText();
        Assert.assertTrue(elementText5.contains("Pop superstar Shakira"));
    }
    @Test(description = "Task1/Part1-Test#3")
    public void inputCategoryLinkToSearchBarAndCheckFirstArticleName () {
        driver.findElement(xpath("//div[@id='orb-nav-links']//a [@href='https://www.bbc.com/news']")).click();
        String elementText =  driver.findElement(xpath(
        "//ul [@class='gs-o-list-ui--top-no-border nw-c-nav__wide-sections']//a[@href='/news/stories']")).getText();
        driver.findElement(xpath("//input[@id='orb-search-q']")).sendKeys(elementText);
        driver.findElement(xpath("//button[@id='orb-search-button']")).click();
        String elementText1 = driver.findElement(xpath("(//a[@class='ssrcss-bxvqns-PromoLink e1f5wbog0'])[1]")).getText();
        Assert.assertTrue(elementText1.contains("Your stories"));
    }
    @Test(description = "Task2/Part2-Test#1")
    public void checkFormToAskYourQuestionWithoutClickOnCheckbox() throws InterruptedException {
        driver.findElement(xpath("//div[@id='orb-nav-links']//a [@href='https://www.bbc.com/news']")).click();
        driver.findElement(xpath("//ul [@class='gs-o-list-ui--top-no-border nw-c-nav__wide-sections']//a[@href='/news/coronavirus']")).click();
        driver.findElement(xpath("//div[@class='gs-u-display-none gs-u-display-block@m']//a [@href='/news/have_your_say']")).click();
        driver.findElement(xpath("//a[@href='/news/52143212']")).click();
        driver.findElement(xpath("//textarea[@class='text-input--long']")).sendKeys("Which one vaccine is better?");
        driver.findElement(xpath("//input[@placeholder='Name']")).sendKeys("Kate");
        driver.findElement(xpath("//input[@placeholder='Email address']")).sendKeys("kdml");
        driver.findElement(xpath("//input[@placeholder='Contact number']")).sendKeys("80501233445");
        driver.findElement(xpath("//input[@placeholder='Location ']")).sendKeys("Ukraine");
        waitForPageLoadComplete(100);
        driver.findElement(xpath("//button[@class='button']")).click();
        waitForPageLoadComplete(100);
        sleep(200);
        String elementText = driver.findElement(xpath("//div[@class='input-error-message']")).getText();
        Assert.assertEquals(elementText, "must be accepted");

    }

    public void waitForPageLoadComplete(long timeToWait) {
        new WebDriverWait(driver, timeToWait).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

    @Test(description = "Task2/Part2-Test#2")
    public void checkFormToAskYourQuestionWithoutContactInfo () throws InterruptedException {
        driver.findElement(xpath("//div[@id='orb-nav-links']//a [@href='https://www.bbc.com/news']")).click();
        driver.findElement(xpath("//ul [@class='gs-o-list-ui--top-no-border nw-c-nav__wide-sections']//a[@href='/news/coronavirus']")).click();
        driver.findElement(xpath("//div[@class='gs-u-display-none gs-u-display-block@m']//a [@href='/news/have_your_say']")).click();
        driver.findElement(xpath("//a[@href='/news/52143212']")).click();
        driver.findElement(xpath("//textarea[@class='text-input--long']")).sendKeys("How often do I need to change mask?");
        driver.findElement(xpath("//input[@placeholder='Name']")).sendKeys("Kira");
        driver.findElement(xpath("//input[@placeholder='Email address']")).sendKeys("kiral");
        driver.findElement(xpath("//input[@placeholder='Contact number']")).sendKeys("80601233445");
        driver.findElement(xpath("//input[@placeholder='Location ']")).sendKeys("Poland");
        sleep(200);
       if (!driver.findElement(xpath("//input[@type='checkbox']")).isSelected()) {
            driver.manage().timeouts().implicitlyWait(100, SECONDS);
            driver.findElement(xpath("//input[@type='checkbox']")).click();
    }
            driver.findElement(xpath("//button[@class='button']")).click();
            waitForPageLoadComplete(100);
            sleep(200);
            String elementText = driver.findElement(xpath("//div[@class='embed-content-container']))")).getText();
            Assert.assertTrue(elementText.contains("Hey Kira, thanks for getting in touch."));
    }

    @Test(description = "Task2/Part2-Test#3")
    public void checkFormToAskYourQuestionWithInvalidEmail() {

    }
    @Test(description = "Task2/Part2-Test#4")
    public void checkFormToAskYourQuestionWithoutQuestion() {

    }
    }

