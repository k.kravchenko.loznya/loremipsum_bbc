package runner;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(
        features = "src/main/resources/bbcTests.feature",
        glue = "stepdefinitions"
)
public class RunnerTestBBC1 extends AbstractTestNGCucumberTests{
}
