package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;
import static org.openqa.selenium.By.xpath;

public class SearchResultsPage extends BasePage {
    private static final String SEARCH_RESULT = "//div[@class = 'prod-cart height']";
    private static final String PRODUCT_STATUS_TEXT = "//div[@class = 'prod-status']";
    private static final String LIST_OF_ELEMENTS ="//div[@class= 'row js_more_content js_height-block']";

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public void setSearchResult(){
        driver.findElement(xpath(SEARCH_RESULT)).click();
}

public String getText() {
        return driver.findElement(xpath(PRODUCT_STATUS_TEXT))
                .getText();
    }

    public int getListSize() {
        List<WebElement> listOfElements = driver.findElements(xpath(LIST_OF_ELEMENTS));
        return listOfElements.size();
    }
}