package avictest;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

public class CheckAvailability extends BasicTest {
    private String SEARCH_KEYWORD = "Razer Kraken Kitty";
    private String AVAILABILITY_OF_PRODUCT = "Есть на складе";

    @Test
    public void checkProductAvailability() {
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
       getSearchResultsPage().setSearchResult();
        String actualAvailability = getSearchResultsPage().getText();
        assertEquals(actualAvailability, AVAILABILITY_OF_PRODUCT);
    }


}
