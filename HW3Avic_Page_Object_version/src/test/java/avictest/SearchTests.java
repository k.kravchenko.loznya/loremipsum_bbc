package avictest;


import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchTests extends BasicTest {
    private String SEARCH_KEYWORD = "Razer Kraken Kitty";
    private String SEARCH_KEYWORD2 = "Моноколесо";
    private String SEARCH_QUERY = "query=Razer+Kraken+Kitty";
    private int QUANTITY_TO_CHECK_WITH = 1;

    @Test
    public void checkThatUrlContainsSearchWord() {
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        assertTrue(getDriver().getCurrentUrl().contains(SEARCH_QUERY));
    }

    @Test
    public void checkAmountOfElementsOnSearchPage() {
        getHomePage().searchByKeyword(SEARCH_KEYWORD2);
        getSearchResultsPage().implicitWait(30);
        int actualListSize = getSearchResultsPage().getListSize();
        assertEquals(actualListSize, QUANTITY_TO_CHECK_WITH);
    }
}
