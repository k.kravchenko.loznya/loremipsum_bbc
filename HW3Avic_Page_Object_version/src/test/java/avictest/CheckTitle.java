package avictest;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckTitle extends BasicTest {
    private final static String TITLE_NAME = "AVIC™ - удобный интернет-магазин бытовой техники и электроники в Украине. | Avic";

    @Test
    public void checkTitle() {
        String actualTitle = getDriver().getTitle();
        String expectedTitle = TITLE_NAME;
        Assert.assertEquals(actualTitle, expectedTitle);
    }

}
